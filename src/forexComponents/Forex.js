import React, { Component } from 'react';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import { Button } from 'reactstrap';
import ForexTable from './ForexTable'
//pikachu

class Forex extends Component {

    state = {
        amount: 0,
        baseCurrency: null,
        targetCurrency: null,
        convertedAmount: 0,
        messageError: '',
        targetCode: null,
        displayRates: null
    }

    handleAmount = e => {
        this.setState({
            amount: e.target.value
        })
    }

    handleBaseCurrency = currency => {
        this.setState({
            baseCurrency: currency
        })
    }

    handleTargetCurrency = currency => {
        this.setState({
            targetCurrency: currency
        })
    }

    handleConvert = () => {
        // const code = this.state.baseCurrency.code;
        // fetch('https://api.exchangeratesapi.io/latest?base=' + code)
        //     .then(res => res.json())
        //     .then(res => {
        //         // console.log(res) output rates
        //         const targetCode = this.state.targetCurrency.code;

        //         const rate = res.rates[targetCode];

        //         // console.log(rate)

        //         this.setState({ convertedAmount: this.state.amount * rate })
        //     })

        //Activity A - Validation
        if (this.state.targetCurrency != null && this.state.baseCurrency != null && this.state.amount > 0) {
            const code = this.state.baseCurrency.code;
            // console.log('hello')
            this.setState({ messageError: "" })
            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
                .then(res => res.json())
                .then(res => {
                    // console.log(res) output rates
                    const targetCode = this.state.targetCurrency.code;

                    const rate = res.rates[targetCode];

                    // console.log(rate)

                    this.setState({ convertedAmount: this.state.amount * rate })

                    this.setState({ targetCode: targetCode })

                })
        } else {
            this.setState({ messageError: "Please fill up with valid data" })
        }


    }

    handleBaseCurrency = currency => {
        this.setState({ baseCurrency: currency });
        this.setState({ code: currency.code });
        const code = this.state.code;

        fetch("https://api.exchangeratesapi.io/latest?base=" + code)
            .then(res => res.json())
            .then(res => {
                const displayRates = res.rates;
                this.setState({ displayRates: displayRates });
            });
    };

    render() {
        // console.log(this.state.amount)
        return (
            <div
                style={{ width: "70%" }}
            >
                <h1 className='text-center my-5'>Forex Calculator</h1>
                <div className='d-flex justify-content-around'
                >
                    <ForexDropdown
                        label={'Base Currency'}
                        onClick={this.handleBaseCurrency}
                        currency={this.state.baseCurrency}
                    />
                    <ForexDropdown
                        label={'Target Currency'}
                        onClick={this.handleTargetCurrency}
                        currency={this.state.targetCurrency}
                    />
                </div>
                <div className="d-flex justify-content-around">
                    <ForexInput
                        label={'Amount'}
                        placeholder={'Amount to convert'}
                        onChange={this.handleAmount}
                    />
                    <Button
                        color='info'
                        onClick={this.handleConvert}
                        handleBaseCurrency={this.handleBaseCurrency}
                    >Convert
                    </Button>
                </div>
                <div>
                    <h1 className='text-center'>{this.state.convertedAmount}{""}{this.state.targetCode}</h1>
                    <h5 className='text-center'>{this.state.messageError}</h5>
                </div>
                <div>
                    <ForexTable

                    />
                </div>
            </div>
        );
    }
}

export default Forex;