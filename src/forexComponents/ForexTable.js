import React, { Component } from 'react';
import Currencies from './ForexData'

class ForexTable extends Component {

    render() {
        return (
            <div>
                <thead>
                    <tr>
                        <th>Currency</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>USD</td>
                        <td>50</td>
                    </tr>
                </tbody>
            </div>
        );
    }
}

export default ForexTable;